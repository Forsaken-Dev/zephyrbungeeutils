package com.zephyrmc.bungeeutils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.SQLDataException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.Callback;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.event.EventHandler;

import com.sk89q.bungee.util.BungeeCommandsManager;
import com.sk89q.bungee.util.CommandExecutor;
import com.sk89q.bungee.util.CommandRegistration;
import com.sk89q.minecraft.util.commands.CommandException;
import com.sk89q.minecraft.util.commands.CommandPermissionsException;
import com.sk89q.minecraft.util.commands.CommandUsageException;
import com.sk89q.minecraft.util.commands.MissingNestedCommandException;
import com.sk89q.minecraft.util.commands.WrappedCommandException;

public class Main extends Plugin implements Listener, CommandExecutor<CommandSender> {

  private Map<String, Boolean>     online   = new HashMap<String, Boolean>();
  public static Main               instance;
  private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
  private Database                 db       = new Database();
  private BungeeCommandsManager    commands;

  public void onEnable() {
    instance = this;
    this.getProxy().getPluginManager().registerListener(this, this);
    this.registerCommands();
    executor.scheduleAtFixedRate(new Runnable() {
      public void run() {
        for (ServerInfo s : ProxyServer.getInstance().getServers().values()) {
          if (!s.getName().equalsIgnoreCase("lobby")) {
            s.ping(new PingCallback(instance, s.getName()));
          }
        }
      }

    }, 0L, 10L, TimeUnit.SECONDS);
  }

  public void onDisable() {
  }

  private void registerCommands() {
    PluginManager pluginManager = this.getProxy().getPluginManager();

    this.commands = new BungeeCommandsManager();

    CommandRegistration registrar = new CommandRegistration(this, pluginManager, this.commands, this);
    registrar.register(ServerCommands.class);
  }

  public void onCommand(CommandSender sender, String commandName, String[] args) {
    try {
      this.commands.execute(commandName, args, sender, sender);
    } catch (CommandPermissionsException e) {
      sender.sendMessage(ChatColor.RED + "You don't have permission.");
    } catch (MissingNestedCommandException e) {
      sender.sendMessage(ChatColor.RED + e.getUsage());
    } catch (CommandUsageException e) {
      sender.sendMessage(ChatColor.RED + e.getMessage());
      sender.sendMessage(ChatColor.RED + e.getUsage());
    } catch (WrappedCommandException e) {
      if (e.getCause() instanceof NumberFormatException) {
        sender.sendMessage(ChatColor.RED + "Number expected, string received instead.");
      } else {
        sender.sendMessage(ChatColor.RED + "An error has occurred. See console.");
        e.printStackTrace();
      }
    } catch (CommandException e) {
      sender.sendMessage(ChatColor.RED + e.getMessage());
    }
  }

  @EventHandler
  public void ProxyPingEvent(ProxyPingEvent ev) {
    ServerPing r = ev.getResponse();
    Byte protocolVersion = Byte.valueOf(r.getProtocolVersion());
    String gameVersion = r.getGameVersion();
    String motd = getMOTD();
    int currentPlayers = Integer.valueOf(r.getCurrentPlayers());
    int maxPlayers = Integer.valueOf(r.getMaxPlayers());
    ServerPing BF = new ServerPing(protocolVersion.byteValue(), gameVersion, motd, currentPlayers, maxPlayers);
    ev.setResponse(BF);
  }

  @EventHandler
  public void PluginMessageEvent(PluginMessageEvent event) {
    if (event.getTag().equals("BungeeCord")) {
      try {
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));
        String subChannel = in.readUTF();
        if (subChannel.equals("CheckServer")) {
          ByteArrayOutputStream b = new ByteArrayOutputStream();
          DataOutputStream out = new DataOutputStream(b);

          String server = in.readUTF();
          if (this.getProxy().getServers().get(server.toLowerCase()) != null) {
            out.writeUTF("CheckServer");
            out.writeUTF(server);
            out.writeBoolean(this.online.get(server));
            this.getProxy().getServers().get("lobby").sendData(event.getTag(), b.toByteArray());
          } else {
            out.writeUTF("CheckServer");
            out.writeUTF(server);
            out.writeBoolean(false);
            this.getProxy().getServers().get("lobby").sendData(event.getTag(), b.toByteArray());
          }
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  @EventHandler
  public void onServerKick(ServerKickEvent event) {
    if (event.getKickReason().contains("restart")) {
      event.setCancelled(true);
      event.setCancelServer(BungeeCord.getInstance().getServerInfo(
          event.getPlayer().getPendingConnection().getListener().getFallbackServer()));
    }
  }

  private int randNum = 2;

  private String getMOTD() {
    String bigColor = "0";
    String littleColor = "0";
    bigColor = randNum + "";
    switch (randNum) {
      case 2:
        littleColor = "a";
        break;
      case 3:
        littleColor = "b";
        break;
      case 4:
        littleColor = "c";
        break;
      case 5:
        littleColor = "d";
        break;
      case 6:
        littleColor = "e";
        break;
    }
    this.randNum++;
    if (randNum % 7 == 0) {
      this.randNum = 2;
    }
    String motd = getDatabaseMOTD().replaceAll("%boldColor%", "&" + bigColor).replaceAll("%accentColor%",
        "&" + littleColor);
    return formatMessage(motd, true);
  }

  private String getDatabaseMOTD() {
    String motd = "    &8&l<&7<&f<    %boldColor%Zephyr&r%accentColor%Unleashed    &f&l>&7>&8>&r              &0|&r";
    DatabaseResults query = this.db.readEnhanced("SELECT `value` FROM `server_extras` WHERE `key` = ?", "server_motd");
    if (query != null && query.hasRows()) {
      try {
        if (!query.getString(0, "value").isEmpty() && query.getString(0, "value") != "") { return query.getString(0,
            "value"); }
      } catch (SQLDataException e) {
        e.printStackTrace();
      }
    }
    return motd;
  }

  private static final char deg           = '\u00A7';
  private static String[][] colourChars   = { { "0", ChatColor.BLACK.toString() },
                                          { "1", ChatColor.DARK_BLUE.toString() },
                                          { "2", ChatColor.DARK_GREEN.toString() },
                                          { "3", ChatColor.DARK_AQUA.toString() },
                                          { "4", ChatColor.DARK_RED.toString() },
                                          { "5", ChatColor.DARK_PURPLE.toString() },
                                          { "6", ChatColor.GOLD.toString() }, { "7", ChatColor.GRAY.toString() },
                                          { "8", ChatColor.DARK_GRAY.toString() },
                                          { "9", ChatColor.BLUE.toString() },
                                          { "a", ChatColor.GREEN.toString() },
                                          { "b", ChatColor.AQUA.toString() },
                                          { "c", ChatColor.RED.toString() },
                                          { "d", ChatColor.LIGHT_PURPLE.toString() },
                                          { "e", ChatColor.YELLOW.toString() }, { "f", ChatColor.WHITE.toString() } };

  private static String[][] modifierChars = { { "l", ChatColor.BOLD.toString() },
                                          { "o", ChatColor.ITALIC.toString() },
                                          { "k", ChatColor.MAGIC.toString() },
                                          { "m", ChatColor.STRIKETHROUGH.toString() },
                                          { "n", ChatColor.UNDERLINE.toString() },
                                          { "r", ChatColor.RESET.toString() } };

  private static String cleanMsgEnding(String msg) {
    while (msg.length() > 0) {
      if (msg.endsWith(String.valueOf(deg)) || msg.endsWith(" ")) {
        msg = msg.substring(0, msg.length() - 1);
      } else if ((msg.length() >= 2) && (msg.charAt(msg.length() - 2) == deg)) {
        msg = msg.substring(0, msg.length() - 2);
      } else {
        break;
      }
    }
    return msg;
  }

  private static String fix(String msg) {
    msg = cleanMsgEnding(msg);
    return msg.trim();
  }

  public static String formatMessage(String msg, boolean formatting) {
    String message = "";
    if (formatting) {
      int applicableModifier = 5;
      textLoop: for (int i = 0; i < msg.length(); i++) {
        char currentLetter = msg.charAt(i);
        if (currentLetter == '&') {
          if (i + 1 < msg.length()) {
            for (String[] s : colourChars) {
              if (msg.toLowerCase().charAt(i + 1) == s[0].charAt(0)) {
                message += s[1];
                if (applicableModifier < 5) {
                  message += modifierChars[applicableModifier][1];
                }
                i++;
                continue textLoop;
              }
            }
            for (int ii = 0; ii < modifierChars.length; ii++) {
              String[] s = modifierChars[ii];
              if (msg.toLowerCase().charAt(i + 1) == s[0].charAt(0)) {
                applicableModifier = ii;
                message += s[1];
                i++;
                continue textLoop;
              }
            }
          }
        }
        message += currentLetter;
      }
    } else {
      message = msg;
      for (String[] s : colourChars) {
        if (msg.contains("&" + s[0])) {
          message = message.replaceAll("&" + s[0], "");
        }
        if (msg.contains("&" + s[0].toUpperCase())) {
          message = message.replaceAll("&" + s[0].toUpperCase(), "");
        }
      }
      for (String[] s : modifierChars) {
        if (msg.contains("&" + s[0])) {
          message = message.replaceAll("&" + s[0], "");
        }
        if (msg.contains("&" + s[0].toUpperCase())) {
          message = message.replaceAll("&" + s[0].toUpperCase(), "");
        }
      }
    }
    return message;
  }

  public class PingCallback implements Callback<ServerPing> {

    private Main   main;
    private String serverName;

    public PingCallback(Main main, String serverName) {
      this.main = main;
      this.serverName = serverName;
    }

    public void done(ServerPing result, Throwable error) {
      if (result != null) {
        main.online.put(serverName.toLowerCase(), true);
      } else {
        main.online.put(serverName.toLowerCase(), false);
      }

    }

  }
}
