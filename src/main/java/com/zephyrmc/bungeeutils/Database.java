package com.zephyrmc.bungeeutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class Database {
  private Connection conn;
  private boolean    showQueries   = false;
  private String     connectionUrl = "sql.zephyr-mc.com";
  private String     databaseName  = "mc";
  private String     username      = "root";
  private String     password      = "bluehungryant";

  public Database() {
    try {
      Class.forName("com.mysql.jdbc.Driver").newInstance();
    } catch (Exception ex) {
      System.out.println("[SEVERE] Failed to initialize JDBC driver");
    }
    try {
      connect();
    } catch (Exception e) {
      System.out.println("[SEVERE] " + e.getMessage());
    }
  }

  public boolean shouldShowQueries() {
    return this.showQueries;
  }

  public void setShowQueries(boolean showQueries) {
    this.showQueries = showQueries;
  }

  private void connect()
      throws Exception {
    try {
      this.conn = DriverManager.getConnection(getConnectionString());
    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println("[SEVERE] Failed to connect to MySQL");
    }
  }

  public void connect(String database) {
    this.databaseName = database;
    try {
      connect();
    } catch (Exception ex) {
      System.out.println("[SEVERE] " + ex.getMessage());
    }
  }

  private void dumpSqlException(SQLException ex) {
    System.out.println("SQLException: " + ex.getMessage());
    System.out.println("SQLState: " + ex.getSQLState());
    System.out.println("VendorError: " + ex.getErrorCode());
    ex.printStackTrace();
  }

  private void ensureConnection() {
    try {
      if ((this.conn == null) || (!this.conn.isValid(5))) try {
        connect();
      } catch (Exception e) {
        if (e.getMessage() != null) System.out.println("[SEVERE] " + e.getMessage());
        else e.printStackTrace();
      }
    } catch (SQLException ex) {
      dumpSqlException(ex);
    }
  }

  public Connection getConn() {
    return this.conn;
  }

  private String getConnectionString() {
    return "jdbc:mysql://" + this.connectionUrl + "/" + this.databaseName + "?user=" + this.username + "&password="
        + this.password;
  }

  private PreparedStatement prepareSqlStatement(String sql, Object[] params)
      throws SQLException {
    PreparedStatement stmt = this.conn.prepareStatement(sql);

    int counter = 1;

    for (Object param : params) {
      if ((param instanceof Integer)) stmt.setInt(counter++, ((Integer) param).intValue());
      else if ((param instanceof Short)) stmt.setShort(counter++, ((Short) param).shortValue());
      else if ((param instanceof Long)) stmt.setLong(counter++, ((Long) param).longValue());
      else if ((param instanceof Double)) stmt.setDouble(counter++, ((Double) param).doubleValue());
      else if ((param instanceof String)) stmt.setString(counter++, (String) param);
      else if (param == null) stmt.setNull(counter++, 0);
      else if ((param instanceof Object)) stmt.setObject(counter++, param);
      else {
        System.out.printf("Database -> Unsupported data type %s", new Object[] { param.getClass().getSimpleName() });
      }
    }
    return stmt;
  }

  public HashMap readRaw(String sql, Object[] params) {
    ensureConnection();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    HashMap Rows = new HashMap();
    try {
      stmt = prepareSqlStatement(sql, params);
      if (stmt.executeQuery() != null) {
        stmt.executeQuery();
        rs = stmt.getResultSet();
        while (rs.next()) {
          ArrayList Col = new ArrayList();
          for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            Col.add(rs.getString(i));
          }
          Rows.put(Integer.valueOf(rs.getRow()), Col);
        }
      }
    } catch (SQLException ex) {
      dumpSqlException(ex);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
        }
        rs = null;
      }
      if (stmt != null) {
        try {
          stmt.close();
        } catch (SQLException sqlEx) {
        }
        stmt = null;
      }
      if ((Rows.isEmpty()) || (Rows == null) || (Rows.get(Integer.valueOf(1)) == null)) { return null; }
    }
    return Rows;
  }

  public DatabaseResults readEnhanced(String sql, Object... params) {
    ensureConnection();
    PreparedStatement stmt = null;
    ResultSet rs = null;
    DatabaseResults results = null;
    try {
      stmt = prepareSqlStatement(sql, params);
      rs = stmt.executeQuery();
      if (rs != null) {
        ResultSetMetaData meta = rs.getMetaData();
        results = new DatabaseResults(meta);
        while (rs.next())
          results.addRow(rs);
      }
    } catch (SQLException ex) {
      dumpSqlException(ex);
    } finally {
      if (rs != null) {
        try {
          rs.close();
        } catch (SQLException sqlEx) {
        }
        rs = null;
      }
      if (stmt != null) {
        try {
          stmt.close();
        } catch (SQLException sqlEx) {
        }
        stmt = null;
      }
      if ((results == null) || (!results.hasRows()) || (results.rawResults == null)) { return null; }
    }
    return results;
  }

  public String tableName(String prefix, String nameOfTable) {
    return String.format("`%s`.`%s`", new Object[] { "mc", prefix + nameOfTable });
  }

  public boolean write(String sql, Object[] params) {
    try {
      ensureConnection();
      PreparedStatement stmt = prepareSqlStatement(sql, params);
      stmt.executeUpdate();
      return true;
    } catch (SQLException ex) {
      dumpSqlException(ex);
    }
    return false;
  }

  public boolean writeNoError(String sql, Object[] params) {
    try {
      ensureConnection();
      PreparedStatement stmt = prepareSqlStatement(sql, params);
      stmt.executeUpdate();
      return true;
    } catch (SQLException ex) {
    }
    return false;
  }
}